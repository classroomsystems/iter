/*
Package iter provides a conventional interface for iterator types.
This interface supports a style of iteration
that is convenient for collating and processing
multiple, ordered, interleaved sequences.

No actual iterators are implemented by this package.
However, the file for_loader.go.tpl is a template for implementing
one kind of iterator using this interface.
The example subdirectory shows a use of this template.
*/
package iter

// Iter is the interface implemented by iterator types.
// There is no method for retrieving the current iterated value.
// That is done in a type-specific way.
type Iter interface {
	// Next advances the Iter to the next element in the
	// sequence. Next must be called before retrieving each
	// value from the sequence, including the first.
	Next()

	// Done returns true when the the iterator has been
	// advanced beyond the end of the sequence. It also
	// returns true when an error has occurred.
	Done() bool

	// Err returns an error that occurred during iteration
	// or nil if there was none.
	Err() error

	// Close terminates iteration. It is idempotent.
	// If Done returns true, the iteration has already been
	// closed, and Close need not be called again.
	Close() error
}

// AnyErr checks whether any of the given Iters has a non-nil error.
// It returns the first error found or nil if none was found.
func AnyErr(s ...Iter) error {
	for i := range s {
		if e := s[i].Err(); e != nil {
			return e
		}
	}
	return nil
}

// AllDone checks whether all the given Iters are done,
// returning true if so.
func AllDone(s ...Iter) bool {
	for i := range s {
		if !s[i].Done() {
			return false
		}
	}
	return true
}
