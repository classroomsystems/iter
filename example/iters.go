/*
DO NOT EDIT
This file was generated with gotyplate.
See https://bitbucket.org/classroomsystems/gotyplate

go generate called at types.go:15
*/

package example

// FooIter iterates over Foo values loaded by a FooLoader.
// FooIters are not safe for concurrent use.
type FooIter struct {
	// Foo is the current value in the iteration.
	// It may be set prior to calling Next to set the initial value
	// passed to LoadFooBatch.
	Foo Foo

	// DoneValue is the value to which Foo
	// will be set when iteration is complete.
	DoneValue Foo

	// Loader is the source from which values are loaded.
	Loader FooLoader

	buf  []Foo
	n    int
	done bool
	err  error
}

// A FooLoader loads batches of Foo values from some source.
// New values should be appended to dest, and the new value of dest returned.
// Prev is the last value already seen – the current value of FooIter.Foo.
// It is useful for keyset paging.
// If LoadFooBatch returns an error or appends no values to dest,
// a FooIter will consider the iteration done and not call the loader again.
type FooLoader interface {
	LoadFooBatch(prev Foo, dest []Foo) ([]Foo, error)
}

// A FooLoadCloser is a FooLoader that must be closed when no longer needed.
// FooIters will call CloseFooBatches whenever iteration is terminated,
// whether or not the iteration was finished normally.
type FooLoadCloser interface {
	FooLoader
	CloseFooBatches() error
}

// Done returns true if there are no more values in the iteration,
// or if an error has occurred.
func (i *FooIter) Done() bool { return i.done }

// Err returns the error that occurred during iteration,
// or nil if there was no error.
func (i *FooIter) Err() error { return i.err }

// Close ends the iteration, closing the underlying FooLoader if necessary.
// If Done() returns true, Close has already been called.
// Close is idempotent and will not attempt to close the loader twice.
func (i *FooIter) Close() error {
	if i.done {
		return i.err
	}
	i.done = true
	if c, ok := i.Loader.(FooLoadCloser); ok {
		if err := c.CloseFooBatches(); err != nil && i.err == nil {
			i.err = err
		}
	}
	return i.err
}

// Next sets i.Foo to the next value in the iteration.
// If there is are no more values in the sequence,
// it sets i.Foo to i.DoneValue.
func (i *FooIter) Next() {
	if i.done {
		i.Foo = i.DoneValue
		return
	}
	if i.n >= len(i.buf) {
		i.n = 0
		i.buf, i.err = i.Loader.LoadFooBatch(i.Foo, i.buf[:0])
		if i.err != nil || len(i.buf) == 0 {
			i.Foo = i.DoneValue
			i.Close()
			return
		}
	}
	i.Foo = i.buf[i.n]
	i.n++
}

// BarIter iterates over Bar values loaded by a BarLoader.
// BarIters are not safe for concurrent use.
type BarIter struct {
	// Bar is the current value in the iteration.
	// It may be set prior to calling Next to set the initial value
	// passed to LoadBarBatch.
	Bar Bar

	// DoneValue is the value to which Bar
	// will be set when iteration is complete.
	DoneValue Bar

	// Loader is the source from which values are loaded.
	Loader BarLoader

	buf  []Bar
	n    int
	done bool
	err  error
}

// A BarLoader loads batches of Bar values from some source.
// New values should be appended to dest, and the new value of dest returned.
// Prev is the last value already seen – the current value of BarIter.Bar.
// It is useful for keyset paging.
// If LoadBarBatch returns an error or appends no values to dest,
// a BarIter will consider the iteration done and not call the loader again.
type BarLoader interface {
	LoadBarBatch(prev Bar, dest []Bar) ([]Bar, error)
}

// A BarLoadCloser is a BarLoader that must be closed when no longer needed.
// BarIters will call CloseBarBatches whenever iteration is terminated,
// whether or not the iteration was finished normally.
type BarLoadCloser interface {
	BarLoader
	CloseBarBatches() error
}

// Done returns true if there are no more values in the iteration,
// or if an error has occurred.
func (i *BarIter) Done() bool { return i.done }

// Err returns the error that occurred during iteration,
// or nil if there was no error.
func (i *BarIter) Err() error { return i.err }

// Close ends the iteration, closing the underlying BarLoader if necessary.
// If Done() returns true, Close has already been called.
// Close is idempotent and will not attempt to close the loader twice.
func (i *BarIter) Close() error {
	if i.done {
		return i.err
	}
	i.done = true
	if c, ok := i.Loader.(BarLoadCloser); ok {
		if err := c.CloseBarBatches(); err != nil && i.err == nil {
			i.err = err
		}
	}
	return i.err
}

// Next sets i.Bar to the next value in the iteration.
// If there is are no more values in the sequence,
// it sets i.Bar to i.DoneValue.
func (i *BarIter) Next() {
	if i.done {
		i.Bar = i.DoneValue
		return
	}
	if i.n >= len(i.buf) {
		i.n = 0
		i.buf, i.err = i.Loader.LoadBarBatch(i.Bar, i.buf[:0])
		if i.err != nil || len(i.buf) == 0 {
			i.Bar = i.DoneValue
			i.Close()
			return
		}
	}
	i.Bar = i.buf[i.n]
	i.n++
}
