package example

// Foo is an example database record type.
type Foo struct {
	ID  int64
	Baz int64
}

// Bar is an example database record type.
type Bar struct {
	ID   int64
	Quux int64
}

//go:generate gotyplate -o iters.go -source bitbucket.org/classroomsystems/iter/for_loader.go.tpl Foo Bar
