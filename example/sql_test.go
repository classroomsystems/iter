package example

import (
	"database/sql"
	"fmt"
	"log"

	"bitbucket.org/classroomsystems/iter"
	"bitbucket.org/classroomsystems/sqldata"

	_ "github.com/mattn/go-sqlite3"
)

var DB *sql.DB

func Example_loadSQL() {
	var inBoth, onlyInFoos, onlyInBars int
	foos := &FooIter{
		Loader:    loader{},
		DoneValue: Foo{ID: maxInt64},
	}
	bars := &BarIter{
		Loader:    loader{},
		DoneValue: Bar{ID: maxInt64},
	}
	foos.Next()
	bars.Next()
	for !iter.AllDone(foos, bars) {
		id := min(foos.Foo.ID, bars.Bar.ID)
		if id == foos.Foo.ID && id == bars.Bar.ID {
			// do some processing
			inBoth++
			foos.Next()
			bars.Next()
		} else if id == foos.Foo.ID {
			// do some processing
			onlyInFoos++
			foos.Next()
		} else {
			// do some processing
			onlyInBars++
			bars.Next()
		}
	}
	if err := iter.AnyErr(foos, bars); err != nil {
		log.Fatalln(err)
	}
	fmt.Println("record counts:", onlyInFoos, inBoth, onlyInBars)
	// Output:
	// loaded 10000 Foo records
	// loaded 10000 Bar records
	// loaded 5079 Foo records
	// loaded 4913 Bar records
	// loaded 0 Bar records
	// loaded 0 Foo records
	// record counts: 7580 7499 7414
}

// A loader type is convenient for grouping related queries.
type loader struct{}

func (loader) LoadFooBatch(prev Foo, dest []Foo) ([]Foo, error) {
	err := sqldata.QueryAll(DB,
		"select {{.}} from Foos where ID > {{$1}} order by ID limit 10000",
		&dest,
		prev.ID,
	)
	fmt.Println("loaded", len(dest), "Foo records")
	return dest, err
}

func (loader) LoadBarBatch(prev Bar, dest []Bar) ([]Bar, error) {
	err := sqldata.QueryAll(DB,
		"select {{.}} from Bars where ID > {{$1}} order by ID limit 10000",
		&dest,
		prev.ID,
	)
	fmt.Println("loaded", len(dest), "Bar records")
	return dest, err
}
