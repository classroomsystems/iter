package example

import (
	"database/sql"
	"log"
	"math/rand"
)

func init() {
	var err error
	DB, err = sql.Open("sqlite3", ":memory:")
	if err != nil {
		log.Fatalln("initializing database", err)
	}
	tx, err := DB.Begin()
	if err != nil {
		log.Fatalln("beginning transaction", err)
	}
	defer tx.Commit()
	x := &errExec{x: tx}
	x.Exec("create table Foos (ID integer primary key, Baz integer)")
	x.Exec("create table Bars (ID integer primary key, Quux integer)")
	rand.Seed(0) // The test needs to be reproducible.
	for id := 1; id < 30000 && x.err == nil; id++ {
		switch rand.Intn(4) {
		case 0:
			// no record
		case 1:
			x.Exec("insert into Foos (ID, Baz) values (?, ?)", id, rand.Int31())
		case 2:
			x.Exec("insert into Bars (ID, Quux) values (?, ?)", id, rand.Int31())
		case 3:
			x.Exec("insert into Foos (ID, Baz) values (?, ?)", id, rand.Int31())
			x.Exec("insert into Bars (ID, Quux) values (?, ?)", id, rand.Int31())
		}
	}
	if x.err != nil {
		log.Fatalln("inserting data", x.err)
	}
}

type errExec struct {
	x interface {
		Exec(query string, args ...interface{}) (sql.Result, error)
	}
	err error
}

func (e *errExec) Exec(query string, args ...interface{}) {
	if e.err != nil {
		return
	}
	_, e.err = e.x.Exec(query, args...)
}

const maxInt64 = 9223372036854775807

func min(a, b int64) int64 {
	if a < b {
		return a
	}
	return b
}
