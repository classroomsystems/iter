{{/*
This template is for use with https://bitbucket.org/classroomsystems/gotyplate
It defines Iters that load batches of values using a loader interface.
*/}}

{{.Package}}

{{.Import}}

{{range .Types}}
// {{.Name}}Iter iterates over {{.Name}} values loaded by a {{.Name}}Loader.
// {{.Name}}Iters are not safe for concurrent use.
type {{.Name}}Iter struct {
	// {{.ShortName}} is the current value in the iteration.
	// It may be set prior to calling Next to set the initial value
	// passed to Load{{.ShortName}}Batch.
	{{.ShortName}} {{.Type}}
	
	// DoneValue is the value to which {{.ShortName}}
	// will be set when iteration is complete.
	DoneValue {{.Type}}
	
	// Loader is the source from which values are loaded.
	Loader {{.Name}}Loader
	
	buf []{{.Type}}
	n int
	done bool
	err error
}

// A {{.Name}}Loader loads batches of {{.Name}} values from some source.
// New values should be appended to dest, and the new value of dest returned.
// Prev is the last value already seen – the current value of {{.Name}}Iter.{{.ShortName}}.
// It is useful for keyset paging.
// If Load{{.ShortName}}Batch returns an error or appends no values to dest,
// a {{.Name}}Iter will consider the iteration done and not call the loader again.
type {{.Name}}Loader interface {
	Load{{.ShortName}}Batch(prev {{.Type}}, dest []{{.Type}}) ([]{{.Type}}, error)
}

// A {{.Name}}LoadCloser is a {{.Name}}Loader that must be closed when no longer needed.
// {{.Name}}Iters will call Close{{.ShortName}}Batches whenever iteration is terminated,
// whether or not the iteration was finished normally.
type {{.Name}}LoadCloser interface {
	{{.Name}}Loader
	Close{{.ShortName}}Batches() error
}

// Done returns true if there are no more values in the iteration,
// or if an error has occurred.
func (i *{{.Name}}Iter) Done() bool { return i.done }

// Err returns the error that occurred during iteration,
// or nil if there was no error.
func (i *{{.Name}}Iter) Err() error { return i.err }

// Close ends the iteration, closing the underlying {{.Name}}Loader if necessary.
// If Done() returns true, Close has already been called.
// Close is idempotent and will not attempt to close the loader twice.
func (i *{{.Name}}Iter) Close() error {
	if i.done {
		return i.err
	}
	i.done = true
	if c, ok := i.Loader.({{.Name}}LoadCloser); ok {
		if err := c.Close{{.ShortName}}Batches(); err != nil && i.err == nil {
			i.err = err
		}
	}
	return i.err
}

// Next sets i.{{.ShortName}} to the next value in the iteration.
// If there is are no more values in the sequence, 
// it sets i.{{.ShortName}} to i.DoneValue.
func (i *{{.Name}}Iter) Next() {
	if i.done {
		i.{{.ShortName}} = i.DoneValue
		return
	}
	if i.n >= len(i.buf) {
		i.n = 0
		i.buf, i.err = i.Loader.Load{{.ShortName}}Batch(i.{{.ShortName}}, i.buf[:0])
		if i.err != nil || len(i.buf) == 0 {
			i.{{.ShortName}} = i.DoneValue
			i.Close()
			return
		}
	}
	i.{{.ShortName}} = i.buf[i.n]
	i.n++
}

{{end}}
